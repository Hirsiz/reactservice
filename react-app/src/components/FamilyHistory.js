// import d3 from 'd3';
import React, { Component } from 'react';
import { FamilyBlock } from './FamilyBlock';
import './FamilyHistory.css';
import { Breadcrumb } from './Breadcrumb';
import { Modal } from './Modal';
import Avatar from './profile-avatar-svgrepo-com.svg'
import Book from './book.svg'
import $ from 'jquery'
import Cookies from 'universal-cookie';
import './Modal.css';


const cookies = new Cookies();

const AddIndividualAPI = 'https://localhost:5001/api/familytree/addindividual';


const API = 'https://localhost:5001/api/familytree?treeid=';

const d3 = require('d3');
var dTree = require('d3-dtree');

var _ = require('lodash');
export class FamilyHistory extends Component {
  displayName = FamilyHistory.name

  constructor(props) {
    super(props);
    if (!cookies.get("name")) {
      window.location.href = "/login";
    }
    this.state = { information: [], loading: true, paths: ["Family History"], seen: false, individualid: null };

    const urlParams = new URLSearchParams(window.location.search);
    const treeid = urlParams.get('treeid');

    fetch(API + treeid)
      .then(response => response.json())
      .then(data => {
        this.setState({ information: data, loading: false });
      });

    

  }

  static togglePop = (e) => {
    $(".modal").css("display", "block");
    $(".close").on("click", () => {
      $(".modal").css("display", "none");
    })

    var parentNode = e.currentTarget.parentElement.parentElement;
    var currentNode = e.currentTarget.parentElement;

    window.parentid = $(parentNode).find('#id').val();
    window.currentid = $(currentNode).find('#id').val();

    console.log(window.parentid);

  };

  static editTile(e) {
    // change the input to inputfields, add save and cancel button
    //if cancel button clicked, go back to previous state
    //if save button clicked, commit changes to db and refresh the page
    var editmode = true;
    var tile = e.target.parentElement.parentElement;

    if (editmode) {
      $(tile).find('.name_input').show();
      $(tile).find('.name_input').css("display", "inline-block");
      $(tile).find('.dob_input').css("display", "inline-block");
      $(tile).find('.dod_input').css("display", "inline-block");

      $(tile).find('.name_input').val($(tile).find('.name').text());
      $(tile).find('.dob_input').val($(tile).find('.dateofbirth').text());
      $(tile).find('.dod_input').val($(tile).find('.dateofdeath').text());

      $(tile).find('.name').hide();
      $(tile).find('.dateofbirth').hide();
      $(tile).find('.dateofdeath').hide();

      $(tile).find('.familyHistory__buttonSave').css("display", "inline-block");
      $(tile).find('.familyHistory__buttonCancel').css("display", "inline-block");
      $(tile).find('.familyHistory__buttonEdit').css("display", "none");
      $(tile).find('.familyHistory__buttonAdd').css("display", "none");
    }
  }

  static cancelTile(e) {
    var tile = e.target.parentElement.parentElement;

    $(tile).find('.name_input').css("display", "none");
    $(tile).find('.dob_input').css("display", "none");
    $(tile).find('.dod_input').css("display", "none");

    $(tile).find('.name').show();
    $(tile).find('.dateofbirth').show();
    $(tile).find('.dateofdeath').show();

    $(tile).find('.familyHistory__buttonSave').css("display", "none");
    $(tile).find('.familyHistory__buttonCancel').css("display", "none");
    $(tile).find('.familyHistory__buttonEdit').css("display", "inline");
    $(tile).find('.familyHistory__buttonAdd').css("display", "inline");

  }

  static saveTile(e) {
    var tile = e.target.parentElement.parentElement;
    console.log(tile);

    var newDetails = {
      name: $(tile).find('.name_input').val(),
      dateofbirth: $(tile).find('.dob_input').val(),
      dateofdeath: $(tile).find('.dod_input').val(),
      id: $(tile).find('#id').val()
    }

    fetch(API,
      {
        method: 'POST',
        body: JSON.stringify(newDetails),
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(
        (res) => res.json()
      ).then(data => {
        window.location.reload();
      })

  }

  // static renderTiles(information) {
  //   // console.log(information);
  //   return (
  //     <div>
  //       <FamilyBlock key={information.id} record={information} />
  //     </div>
  //   );
  // }



  static createButtons() {
    return "<div class='familyHistory_buttons'>" +
      "<button class='familyHistory__buttonEdit'> Edit </button> " +
      "<button class='familyHistory__buttonAdd'> Add </button> " +
      "<button class='familyHistory__buttonSave' hidden='true' > Save </button> " +
      "<button class='familyHistory__buttonCancel' hidden='true'> Cancel </button>" +
      "</div>";
  }

  renderTree() {

    window.d3 = d3;


    var treeData = this.state.information;

    var treeDataCollection =[];
    treeDataCollection.push(treeData);
    console.log(treeDataCollection);
    
    return dTree.init(treeDataCollection, {
      target: "#graph",
      debug: true,
      height: 800,
      width: 1350,
      callbacks: {
        nodeClick: function (name, extra, id) {

        },
        textRenderer: function (name, extra, textClass, id) {
          // THis callback is optinal but can be used to customize
          // how the text is rendered without having to rewrite the entire node
          // from screatch.

          var name = "<div align='center' class='" + textClass + " name familyHistory_name'>" + name + "</div>"

          var dateOfBirth;
          var dateOfDeath;
          var id;
          if (extra) {
            dateOfBirth = "<div align='center' class='" + textClass + " dateofbirth familyHistory_text'>" + extra.dateOfBirth + "</div>";
            dateOfDeath = "<div align='center' class='" + textClass + " dateofdeath familyHistory_text'>" + extra.dateOfDeath + "</div>";
            id = "<input type='hidden' id='id' value='" + extra.id + "'/>";
          }
          var image = "<div><img src='" + Avatar + "' class='familyHistory_Image'/></div>"
          var button = FamilyHistory.createButtons();



          //add a hidden element containing the id of the individual
          var inputName = "<input type='text' class='name_input' hidden='true' />"
          var inputDateOfBirth = "<input type='text' class='dob_input' hidden='true' />"
          var inputDateOfDeath = "<input type='text' class='dod_input' hidden='true' />"


          return "<div>" + name + image + inputName + dateOfBirth + inputDateOfBirth + dateOfDeath + inputDateOfDeath + id + button + "</div>";
        },
        nodeRenderer: function (name, x, y, height, width, extra, id, nodeClass, textClass, textRenderer) {
          // This callback is optional but can be used to customize the
          // node element using HTML.

          let node = '';
          node += '<div ';
          node += 'style="height:100%;width:100%;" ';
          node += 'class="' + nodeClass + '" ';
          node += 'id="node' + id + '">\n';
          node += textRenderer(name, extra, textClass, id);
          node += '</div>';

          $(".familyHistory__buttonEdit").off().on('click', FamilyHistory.editTile);
          $(".familyHistory__buttonCancel").off().on('click', FamilyHistory.cancelTile);
          $(".familyHistory__buttonSave").off().on('click', FamilyHistory.saveTile);
          $(".familyHistory__buttonAdd").off().on('click', FamilyHistory.togglePop);

          // $(".familyHistory__buttonAdd").off().on('click', function (e) {

          // });

          return node;
        }
      }
    });
  }

  async submit(event) {
    event.preventDefault();

    var targetindividualid;

    if (event.target.elements.individualtype.value == 'spouse') {
      targetindividualid = window.currentid;
    } else if (event.target.elements.individualtype.value == 'child') {
      targetindividualid = window.parentid;
    }

    var formdata = {
      name: event.target.elements.name.value,
      individualtype: event.target.elements.individualtype.value,
      placeofbirth: event.target.elements.placeofbirth.value,
      dateofbirth: event.target.elements.dateofbirth.value,
      dateofdeath: event.target.elements.dateofdeath.value,
      gender: event.target.elements.individualgender.value,
      targetindividualid: targetindividualid
    }
    fetch(AddIndividualAPI, {
      method: 'POST',
      body: JSON.stringify(formdata),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    console.log(formdata);
  }



  render() {
    // let contents = this.state.loading
    //   ? <p><em>Loading...</em></p>
    //   : FamilyHistory.renderTiles(this.state.information);

    return (
      <div id="mainContent">
        <div>
          <Breadcrumb record={this.state.paths} />
        </div>
        <div class="modal">
          <div class="modal_content">
            <span class="close">&times;</span>
            <p>Add Relative</p>
            <form onSubmit={event => this.submit(event)}>
              <div class="modal_fields">
                <div class="modal_keyValue">
                  <label class="modal_label">
                    Select Type
                                </label>
                  <select id="individualtype" name="individualtype" class="modal_dropdown">
                    <option value="child">Child</option>
                    <option value="spouse">Spouse</option>
                  </select>
                </div>
                <div class="modal_keyValue">
                  <label class="modal_label">
                    Name
                                </label>
                  <input id="name" name="name" type='text' class="modal_field">
                  </input>
                </div>
                <div class="modal_keyValue">
                  <label class="modal_label">
                    Place of Birth
                                </label>
                  <input id="placeofbirth" name="placeofbirth" type='text' class="modal_field">
                  </input>
                </div>
                <div class="modal_keyValue">
                  <label class="modal_label">
                    Date of Birth
                                </label>
                  <input id="dateofbirth" name="dateofbirth" type='text' class="modal_field">
                  </input>
                </div>
                <div class="modal_keyValue">
                  <label class="modal_label">
                    Date of Death
                                </label>
                  <input id="dateofdeath" name="dateofdeath" type='text' class="modal_field">
                  </input>
                </div>
                <div class="modal_keyValue">
                  <label class="modal_label">
                    Gender
                                </label>
                  <select class="modal_dropdown" name="individualgender">
                    <option name="dropdown" value="male">Male</option>
                    <option name="dropdown" value="female">Female</option>
                    <option name="dropdown" value="unknown">Unknown</option>
                  </select>
                </div>
              </div>
              <input type="hidden" value={this.props.individualid} name="targetindividualid" id="targetindividualid" />
              <button type="submit">Save</button>
            </form>
          </div>
        </div >
        <div id="graph">{this.renderTree()}</div>
      </div>
    );

  }
}