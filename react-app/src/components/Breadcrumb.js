import React, { Component } from 'react';
import './Breadcrumb.css';

export class Breadcrumb extends Component {
  displayName = Breadcrumb.name


  render() {
    let paths = this.props.record;
        return (
      <div id="breadcrumb">
        {paths.map(path => <span>Home > {path}</span>)}
      </div>
    );
  }

}