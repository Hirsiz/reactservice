import React, { Component } from 'react';
import './Modal.css';
import { Link } from 'react-router-dom';

const AddIndividualAPI = 'https://localhost:5001/api/familytree/addindividual';

export class Modal extends Component {
    displayName = Modal.name

    constructor(props) {
        super(props);
        console.log(props.individualid);

    }

    async submit(event) {
        event.preventDefault();

        var formdata = {
            name: event.target.elements.name.value,
            individualtype: event.target.elements.individualtype.value,
            placeofbirth: event.target.elements.placeofbirth.value,
            dateofbirth: event.target.elements.dateofbirth.value,
            dateofdeath: event.target.elements.dateofdeath.value,
            gender: event.target.elements.individualgender.value,
            individualid: event.target.elements.targetindividualid.value
        }
        fetch(AddIndividualAPI, {
            method: 'POST',
            body: JSON.stringify(formdata),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        console.log(formdata);
    }


    // render() {
    //     return (
            
    //     );
    // }

}
