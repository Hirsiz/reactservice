import React, { Component } from 'react';
import './Landing.css';
import People from './andre-hunter-xpxKHXmxwOM-unsplash.jpg';
import Book from './wasabi book.png';
import Tree from './tree_divider.png'
import { Link } from 'react-router-dom';

export class Landing extends Component {
    displayName = Landing.name

    constructor(props) {
        super(props);
    }



    render() {
        return (
            <div>
                <div id='landing_title'>
                    All Hands <br /> on deck!
                </div>

                <div id='landing_title_caption'>
                    Visualise, process and transform data
                </div>

                <div id='landing_intro'>
                    <div id='landing_intro_text'>
                        <div id='landing_intro_text_title'>
                            <b>Communicating</b> with people in your team
                        </div>
                        <div id='landing_intro_text_content'>
                            Lorem ipsum dolor ipsum aaa aaaLorem ipsum dolor ipsum Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum
                        </div>
                    </div>
                    <div >
                        <img id='landing_intro_image' src={People} />
                    </div>
                </div>


                <div id='landing_features_outer'>
                    <div id='landing_features'>
                        <div id='landing_features_image'>
                            <img id='landing_features_image_cropped' src={Book} />
                        </div>
                        <div id='landing_features_text'>
                            <div id='landing_features_text_title'>
                                Take your studio with you wherever you go
                            </div>
                            <div id='landing_features_text_caption'>
                                <ul>
                                    <li>20 hours of ready made videos</li>
                                    <li>Create unlimited custom classes with full videos</li>
                                    <li>Over 280 poses with detailed animation</li>
                                    <li>Perfect for beginners, awesome for experts</li>
                                    <li>Developed and produced by qualified instructor</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                <div id='landing_pupose'>
                    <div id='landing_pupose_benefits'>
                        <div id='landing_pupose_benefits_title'>
                            Benefits
                        </div>
                        <div id='divider'></div>
                        <div id='landing_pupose_benefits_content'>
                            Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum
                        </div>
                    </div>
                    <div id='landing_pupose_motivation'>
                        <div id='landing_pupose_benefits_title'>
                            Motivations
                        </div>
                        <div id='divider'></div>
                        <div id='landing_pupose_benefits_content'>
                            Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum Lorem ipsum dolor ipsum
                        </div>
                    </div>
                </div>

                <div >
                    <img src={Tree} id='landing_divider' />
                </div>

                <div id='landing_register'>
                    <Link to="/createaccount">
                        <button id='landing_register_button'>Register</button>
                    </Link>
                </div>
            </div>
        );
    }
}
