import React, { Component } from 'react';
import { NavMenu } from './NavMenu';

export class Layout extends Component {
  displayName = Layout.name

  constructor(props) {
    super(props);
  }

  render() {
    console.log(window.location.pathname);
    var menu = window.location.pathname == '/login' ||  window.location.pathname == '/createaccount' ? null : <NavMenu />;
    return (
      <div>
        {menu}
        {this.props.children}
      </div>
    );
  }
}
