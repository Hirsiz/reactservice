import React, { Component } from 'react';
import './ShortTile.css';
import { Link } from 'react-router-dom';

export class ShortTile extends Component {
    displayName = ShortTile.name

    familyHistory() {
        window.location.href = '/familyhistory?treeid=' + this.props.record.id;
    }


    render() {
        var families = this.props.record;
        return (
            <div id="shortTile" onClick={() => this.familyHistory()}>
                <div id="shortTile_name">
                    <div id="shortTile_keyValue">
                        <div id="shortTile_key">
                            Name
                        </div>
                        <div id="shortTile_value">
                            {families.family[0].father.name} Family Tree
                            {
                                //replace with familytreename
                            }
                        </div>
                    </div>
                </div>

                <div id="shortTile_information">
                    <div id="shortTile_keyValue">
                        <div id="shortTile_key">
                            Males
                        </div>
                        <div id="shortTile_value">
                            {families.information.maleCount}
                        </div>
                    </div>
                    <div id="shortTile_keyValue">
                        <div id="shortTile_key">
                            Females
                        </div>
                        <div id="shortTile_value">
                            {families.information.femaleCount}
                        </div>
                    </div>
                    <div id="shortTile_keyValue">
                        <div id="shortTile_key">
                            Unknown
                        </div>
                        <div id="shortTile_value">
                            {families.information.unknownCount}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}