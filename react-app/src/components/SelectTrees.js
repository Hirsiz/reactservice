import React, { Component } from 'react';
import { post } from 'axios';
import './SelectTrees.css';
import Cookies from 'universal-cookie';
import { Link } from 'react-router-dom';
import $ from 'jquery'
import { tree } from 'd3';


const cookies = new Cookies();
const GetFilesAPI = 'https://localhost:46397/api/import?'


export class SelectTrees extends Component {

    constructor(props) {
        super(props);
        this.state = { files: [] };

        if (!cookies.get("name")) {
            window.location.href = "/login";
        }

        fetch(GetFilesAPI + "accesstoken=" + cookies.get("name").accessToken)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({ files: data });
            })

    }

    async submit(event) {
        // event.preventDefault();
        // let formData = new FormData();
        // formData.append('body', this.state.file);
        // formData.append('accesstoken', cookies.get("name").accessToken);
        // formData.append('treeName', event.target.elements.fileInput_treename.value);
        // return post(ImportAPI, formData);
    }

    renderMergeOptions() {
        return this.state.files.map((file, index) => {
            const { id, name, treeName } = file;
            return (
                <option value={id}>{treeName}</option>
            )
        })
    }

    submit(e) {
        e.preventDefault();
        console.log(e);
        var trees = {
            tree1: e.target.elements.tree1.value,
            tree2: e.target.elements.tree2.value
        }
        window.location.href = '/mergetrees?tree1=' + trees.tree1 + '&tree2=' + trees.tree2;
    }

    render() {
        return (
            <div>
                <div id='mergeTree__form'>
                    <form onSubmit={event => this.submit(event)}>
                        {/* <label>Maintainer</label> */}
                        <select id="mergeTree_list" name="tree1">
                            {this.renderMergeOptions()}
                        </select>

                        {/* <label>Maintainer</label> */}
                        <select id="mergeTree_list" name="tree2">
                            {this.renderMergeOptions()}
                        </select>
                        <button id='mergeTree__button' type="submit">Merge</button>
                    </form>
                </div>
            </div>
        )
    }
}