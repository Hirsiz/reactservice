import React, { Component } from 'react';
import './CreateAccount.css';
import Building from './bady-qb-hxi_yRxODNc-unsplash.jpg';
import { Link } from 'react-router-dom';
import logo from './noun_Pen_2144033 green.svg'


const AddUserAPI = 'https://localhost:44556/api/authentication/createuser';

export class CreateAccount extends Component {
    displayName = CreateAccount.name


    submitNewUser(event) {
        event.preventDefault();

        var formdata = {
            firstname: event.target.elements.firstname.value,
            lastname: event.target.elements.lastname.value,
            username: event.target.elements.username.value,
            password: event.target.elements.password.value
        }
        fetch(AddUserAPI, {
            method: 'POST',
            body: JSON.stringify(formdata),
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    redirectToLogin() {
        window.location.href = '/login';
    }



    render() {
        return (
            <div class="createAccount_container" >
                <div class='createAccount_content'>
                    <div>
                        <Link id="createAccount_logo" to="/">
                            <img src={logo} alt="logo" id="logo" />
                            INDIVIDUAL PROJECT
                        </Link>
                        <div class='createAccount_logo_caption'>
                            Welcome to individual project
                        </div>
                    </div>
                    <form class='create_accountForm' onSubmit={event => this.submitNewUser(event)}>
                        <div>
                            <label class='createAccount_form_label'>Username</label>
                            <input class='createAccount_form_input_field' type="text" id="username" name="username" />
                        </div>
                        <div>
                            <label class='createAccount_form_label'>Password</label>
                            <input class='createAccount_form_input_field' type="text" id="password" name="password" />
                        </div>
                        <div>
                            <label class='createAccount_form_label'>Firstname</label>
                            <input class='createAccount_form_input_field' type="text" id="firstname" name="firstname" />
                        </div>
                        <div>
                            <label class='createAccount_form_label'>Lastname</label>
                            <input class='createAccount_form_input_field' type="text" id="lastname" name="lastname" />
                        </div>
                        <div>
                            <input class='createAccount_form_button' type="submit" value="Create account" />
                            <button class='createAccount_form_button' type="submit" onClick={() => this.redirectToLogin()}>Login</button>
                        </div>

                    </form>
                </div>
                <div class='createAccount_image'>
                    <img class='createAccount_image_conent' src={Building}></img>
                </div>
            </div >

        )
    }
}