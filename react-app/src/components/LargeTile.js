import React, { Component } from 'react';
import './LargeTile.css';
import { Link } from 'react-router-dom';

export class LargeTile extends Component {
    displayName = LargeTile.name

    render() {
        return (
            <Link id="largeTile" to={this.props.link}>
                <div>
                    <div id="largeTileHeader">
                        {this.props.header}
                    </div>
                    <div>
                        <img id="largeTileImage" src={this.props.image} />
                    </div>
                    <div id='largeTile_divider'></div>
                    <div id='largeTile_caption'>
                        Import family trees into the system. 
                        <br />
                        <br />
                        Manage maintainability and visibility of the tree
                    </div>

                </div>
            </Link>

        );
    }

}
