import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Login.css';
import logo from './noun_Pen_2144033 green.svg'
import oldman from './mile-modic-Sv9xCQ7RBCs-unsplash.jpg';
import Cookies from 'universal-cookie';
import $ from 'jquery'

const CheckUserAPI = 'https://localhost:44556/api/authentication?';
const cookies = new Cookies();

const validateForm = (errors) => {
    let valid = true;
    Object.values(errors).forEach(
        // if we have an error string set valid to false
        (val) => val.length > 0 && (valid = false)
    );
    return valid;
}

export class Login extends Component {
    displayName = Login.name

    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null,
            errors: {
                username: '',
                password: ''
            }
        };
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'username':
                if (value.length < 5) {
                    errors.username = 'Full Name must be 5 characters long!'
                    $('.login_form_input_username_error').css('visibility', 'visible');
                } else {
                    errors.username = '';
                }
                break;

            case 'password':
                if (value.length < 4) {
                    errors.password = 'Password must be 4 characters long!';
                    $('.login_form_input_password_error').css('visibility', 'visible');
                } else {
                    errors.password = '';
                }
                break;

            default:
                break;
        }

        this.setState({ errors, [name]: value });
    }

    submitUser(event) {
        event.preventDefault();

        if (validateForm(this.state.errors)) {
            var formdata = {
                username: event.target.elements.username.value,
                password: event.target.elements.password.value
            }

            fetch(CheckUserAPI + "username=" + formdata.username + "&password=" + formdata.password, {
                method: 'GET',
            }).then(response => {
                const jsonResponse = response.json();
                jsonResponse.then(data => {
                    if (data) {
                        console.log(data);
                        cookies.set('name', data, { path: '/', maxAge: 20000 });
                        cookies.set('accesstoken', data.accessToken, { path: '/', maxAge: 20000 });
                        window.location.href = "/"
                    }
                }).catch(error => {
                    console.log("Unsuccessful request, Could not parse body as json", error);
                    $('.login_form_incorrect').css('visibility', 'visible');
                })
            })
        } else {
            console.log('Invalid Form');
            
        }
    }

    render() {
        const { errors } = this.state;
        return (
            < div class='login_container'>
                <div class='login_image'>
                    <img class='login_image_conent' src={oldman}></img>
                </div>
                <div class='login_content'>
                    <div>
                        <Link id="login_logo" to="/">
                            <img src={logo} alt="logo" id="logo" />
                            INDIVIDUAL PROJECT
                        </Link>
                        <div class='login_logo_caption'>
                            Welcome to individual project
                        </div>
                    </div>
                    <form class='login_form' onSubmit={event => this.submitUser(event)}>
                        <div>
                            <label class='login_form_label'>Username</label>
                            <input class='login_form_input_username' type="text" id="username" name="username" onChange={this.handleChange} />
                            <div className='login_form_input_username_error'>{errors.username}</div>
                        </div>
                        <div>
                            <label class='login_form_label'>Password</label>
                            <input class='login_form_input_password' type="password" id="password" name="password" onChange={this.handleChange} />
                            <div className='login_form_input_password_error'>{errors.password}</div>
                        </div>
                        <div class='login_form_createaccount'>Dont have an account? <Link to="/createaccount">Register</Link></div>
                        <div>
                            <input class='login_form_button' type="submit" value="Sign in" />
                        </div>
                        <div class='login_form_incorrect'>
                            Incorrect Username or password!
                        </div>
                    </form>
                </div>
            </div >

        )
    }
}