import React, { Component } from 'react';
import './Home.css';
import { Link } from 'react-router-dom';
import { LargeTile } from './LargeTile';
import Arrow from './arrow.svg'
import Tree from './noun_Tree_2878883.svg'
import Book from './book.svg'
import Users from './noun_User_3126188.svg'

import Cookies from 'universal-cookie';

const cookies= new Cookies();

export class Home extends Component {
  displayName = Home.name

  constructor(props) {
    super(props);
    if (!cookies.get("name")) {
      window.location.href = "/login";
    }
  }



  render() {
    return (
      <div id="homeMainContent">
        <div id="homeTitle">
    <div id="homeMainTitle">Welcome {cookies.get('name').firstname}</div>
          <div id="homeSubTitle">Choose Your Options</div>
        </div>

        <div id="homeBox">
          <LargeTile header="Manage Trees" image={Arrow} link="import"/>
          <LargeTile header="View Family History" image={Tree} link="familytreeselector"/>
          <LargeTile header="Book Production" image={Book} link="bookproduction"/>
          <LargeTile header="Manage Users" image={Users} link="users"/>
        </div>
      </div>
    );
  }
}
