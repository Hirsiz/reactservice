import Cookies from 'universal-cookie';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';

const cookies = new Cookies();

export class Logout extends Component {
    displayName = Logout.name

    constructor(props){
        super(props);
        cookies.remove('name');
        cookies.remove('accesstoken');
        console.log("logout");
        window.location.href = "/landing"
    }

    render(){
        return(
            <Link to='/landing'/>
        )
    }
}