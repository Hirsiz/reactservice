import React, { Component } from 'react';
import './GroupTable.css';
import Bin from './bin.svg';
import $ from 'jquery';
import Cookies from 'universal-cookie';


const UserGroupAPI = 'https://localhost:44556/api/usergroup/';
const cookies = new Cookies();

export class GroupTable extends Component {
    displayName = GroupTable.name

    constructor(props) {
        super(props);
        this.state =
        {
            groups: [],
            some: ''
        };
        this.setState({ groups: this.props.groups })
        if (!cookies.get("name")) {
            window.location.href = "/login";
        }
    }

    renderTableData() {
        var ID = 1;
        if (this.props.groups) {
            return this.props.groups.map((group, index) => {
                const { id, name, users } = group;
                return (
                    <tr class='groupTable__row'>
                        <td id='groupTable__content'>{ID++}</td>
                        <td id='groupTable__content'>{name}</td>
                        <td id='groupTable__content_clickable' onClick={(e) => this.updateUserGroupSelection(e, index)}>View And Edit Permissions</td>
                        <td id='groupTable__content'><img id='table__image' src={Bin} onClick={() => this.deleteRow(id)} /></td>
                    </tr>
                )
            })
        }
    }

    updateUserGroupSelection(event, index) {
        this.props.onGroupSelected(index);

        var row = event.target.parentElement;

        $(".groupTable__row").removeClass("groupTable__row__selected");
        $(row).addClass("groupTable__row__selected");
    }

    async deleteRow(id) {
        const response = await fetch(UserGroupAPI + "deleteusergroup", {
            method: 'delete',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(id)
        });
        const data = await response.json();
        this.props.onGroupChanged(data);
    }


    submitNewUserGroup(event) {
        event.preventDefault();
        var formData = {
            name: event.target.elements.name.value,
            accessToken: cookies.get("name").accessToken
        }

        fetch(UserGroupAPI + "addusergroup", {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(data => {
                this.props.onGroupChanged(data);
            })
    }

    renderNewUserGroup() {
        $("#groupTable_form").css("display", "block");
    }

    render() {
        return (
            <table id='groupTable'>
                <div id='groupTable__title'>
                    All User groups
                </div>

                <tbody>
                    <tr>
                        <th id='groupTable__header'>ID</th>
                        <th id='groupTable__header'>Group</th>
                        <th id='groupTable__header'>Permissions</th>
                        <th id='groupTable__header'>Delete</th>
                    </tr>
                </tbody>
                {this.renderTableData()}
                <div onClick={(e) => this.renderNewUserGroup()}>Add new group</div>
                <div>
                    <form id='groupTable_form' onSubmit={event => this.submitNewUserGroup(event)}>
                        <input type='text' id='name' name='name' />
                        <button type='submit'>Submit</button>
                    </form>
                </div>
            </table>
        )
    }
}