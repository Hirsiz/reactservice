import React, { Component } from 'react';
import './LongTile.css';
import { Link } from 'react-router-dom';

const API = 'https://localhost:12345/api/bookproduction/download';
const FileDownload = require('js-file-download');

export class LongTile extends Component {
    displayName = LongTile.name


    static downloadPDF(id) {
        fetch(API + "/" + id)
            .then(response => response.json())
            .then(data => {
                var byteArray= this.base64ToArrayBuffer(data.fileContents);
                FileDownload(byteArray, data.fileDownloadName);
            });
    }


    //Cannot create a pdf file without changing the base64 byte array to an array buffer
    static base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
            var ascii = binaryString.charCodeAt(i);
            bytes[i] = ascii;
        }
        return bytes;
    }


    render() {
        var families = this.props.record;
        console.log(families);
        return (
            <div id="longTile">
                <div id="longTile_name">
                    <div id="longTile_keyValue">
                        <div id="longTile_key">
                            Name
                    </div>
                        <div id="longTile_value">
                            {families.family[0].father.name} Family Tree
                        </div>
                    </div>
                </div>

                <div id="longTile_information">
                    <div id="longTile_keyValue">
                        <div id="longTile_key">
                            Males
                    </div>
                        <div id="longTile_value">
                            {families.information.maleCount}
                        </div>
                    </div>
                    <div id="longTile_keyValue">
                        <div id="longTile_key">
                            Females
                    </div>
                        <div id="longTile_value">
                            {families.information.femaleCount}
                        </div>
                    </div>
                    <div id="longTile_keyValue">
                        <div id="longTile_key">
                            Unknown
                        </div>
                        <div id="longTile_value">
                            {families.information.unknownCount}
                        </div>
                    </div>

                    <div id="longTile_keyValue">
                        <div onClick={() => LongTile.downloadPDF(families.id)} id="longTile_download">
                            Download
                        </div>
                    </div>
                </div>

            </div>
        );
    }

}
