import React, { Component } from 'react';
import './Home.css';
import { Link } from 'react-router-dom';
import { LongTile } from './LongTile';
import './BookProduction.css';
import { Breadcrumb } from './Breadcrumb';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

const API = 'https://localhost:12345/api/bookproduction';

export class BookProduction extends Component {
    displayName = BookProduction.name

    constructor(props) {
        super(props);
        if (!cookies.get("name")) {
            window.location.href = "/login";
        }
        this.state = { information: [], paths: ["Books"] };

        fetch(API)
            .then(response => response.json())
            .then(data => {
                this.setState({ information: data, loading: false });
            })
    }


    static renderTiles(information) {
        return (
            <div>
                {information.map(info => <LongTile record={info} />)}
            </div>
        );
    }

    render() {
        return (
            <div id="bookProduction_MainContent">
                <div>
                    <Breadcrumb record={this.state.paths} />
                </div>

                <div id="bookProduction_Title">
                    Choose your Book to Download
                </div>

                {BookProduction.renderTiles(this.state.information)}

                <div>
                </div>
            </div>
        );
    }
}
