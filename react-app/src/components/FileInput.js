import React, { Component } from 'react';
import { post } from 'axios';
import './FileInput.css';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
const ImportAPI = 'https://localhost:46397/api/import';
const UserGroupsAPI = 'https://localhost:44556/api/usergroup/'

const validateForm = (errors) => {
    let valid = true;
    Object.values(errors).forEach(
        // if we have an error string set valid to false
        (val) => val.length > 0 && (valid = false)
    );
    return valid;
}

export class FileInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: null,
            errors: {
                treename: '',
                file: '',
                permissionIDs: '',
                visibilityIDs: ''
            },
            groups: []
        };

        fetch(UserGroupsAPI + "getusergroups?accesstoken=" + cookies.get("name").accessToken)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({ groups: data });
            })

    }

    getPermissionIDs() {
        this.state.groups.map(group => {
            this.setState.permissionIDs.push(group.user)
        })
    }

    getPermissionIDs() {
        this.state.groups.map(group => {
            this.setState.permissionIDs.push(group.user)
        })
    }


    async submit(event) {
        event.preventDefault();
        //store permission and visibilityIDs in object and send them to
        this.handleChange(event);
        if (validateForm(this.state.errors)) {
            let formData = new FormData();
            formData.append('body', this.state.file);
            formData.append('accesstoken', cookies.get("name").accessToken);
            formData.append('treeName', event.target.elements.fileInput_treename.value);
            formData.append('maintainerID', event.target.elements.maintainerGroup.value);
            formData.append('visibilityID', event.target.elements.visibilityGroup.value);

            return post(ImportAPI, formData)
                .then(response => {
                    this.props.onFileChange(response.data);
                });
        } else {
            console.log('Invalid Form');
            console.log(this.state.errors);
        }
    }

    setFile(event) {
        this.setState({ file: event.target.files[0] });
        // this.handleChange(event);
    }

    handleChange = (event) => {
        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'treename':
                if (value.length < 3) {
                    errors.treename = 'Must input a tree name!'
                    // $('.login_form_input_username_error').css('visibility', 'visible');
                    console.log(errors.treename);
                } else {
                    errors.treename = '';
                }
                break;

            case 'file':
                if (this.state.file == null) {
                    errors.file = 'Must input a file!';
                    // $('.login_form_input_password_error').css('visibility', 'visible');
                } else {
                    console.log(value);
                    errors.file = '';
                }
                break;

            default:
                break;
        }

        this.setState({ errors, [name]: value });
    }

    renderGroupOptions() {
        return this.state.groups.map((group, index) => {
            const { id, name } = group;
            return (
                <option value={id}>{name}</option>
            )
        })
    }

    render() {
        return (
            <form id='fileInput__form' onSubmit={event => this.submit(event)}>
                <div>
                    <div>
                        <label class='fileInput_label'>Tree name</label>
                    </div>
                    <input type="text" id="fileInput_treename" placeholder="Treename" name='treename' onChange={this.handleChange} />
                </div>

                <div>
                    <div>
                        <label class='fileInput_label'>Maintainer</label>
                    </div>
                    <select id="fileInput_list" name='maintainerGroup' placeholder="Maintainer">
                        <option value={0}>None</option>
                        {this.renderGroupOptions()}
                    </select>
                </div>
                <div>
                    <div>
                        <label class='fileInput_label'>Visible By</label>
                    </div>
                    <select id="fileInput_list" name='visibilityGroup' placeholder="Visibility">
                        <option value={0}>None</option>
                        {this.renderGroupOptions()}
                    </select>
                </div>

                <div id='fileInput_buttons'>
                    <label for="fileInput_upload" id='fileInput_styledUpload'>Choose</label>
                    <input type="file" onChange={(event) => this.setFile(event)} name='file' id="fileInput_upload" />
                    <br />
                    <button id='fileInput__button' type="submit">Upload</button>
                </div>

            </form>
        );
    }
}