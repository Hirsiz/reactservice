import React, { Component } from 'react';
import './Table.css';
import Bin from './bin.svg'


const ImportAPI = 'https://localhost:46397/api/import';

export class Table extends Component {
    displayName = Table.name

    constructor(props) {
        super(props);
        this.state = { files: [] };
        this.setState({ files: this.props.files });
    }

    renderTableData() {
        var ID = 1;
        return this.props.files.map((file, index) => {
            const { id, name, treeName } = file;
            console.log(file);
            return (
                <tr key={id}>
                    <td id='table__content'>{ID++}</td>
                    <td id='table__content'>{name}</td>
                    <td id='table__content'>{treeName}</td>
                    <td id='table__content'><img id='table__image' src={Bin} onClick={() => this.deleteRow(id)} /></td>
                </tr>
            )
        })
    }

    deleteRow(id) {
        return fetch(ImportAPI, {
            method: 'delete',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(id)
        })
        .then(response => response.json())
        .then(data => {
            this.props.onFileChange(data);
        });
    }


    render() {
        return (
            <table id='table'>
                <tbody>
                    <tr>
                        <th id='table__row'>ID</th>
                        <th id='table__row'>FileName</th>
                        <th id='table__row'>TreeName</th>
                        <th id='table__row'>Delete</th>
                    </tr>
                </tbody>
                {this.renderTableData()}
            </table>
        );
    }
}