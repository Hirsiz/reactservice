import React, { Component } from 'react';
import './UserTable.css';
import Bin from './bin.svg';

const UserGroupAPI = 'https://localhost:44556/api/usergroup/';

export class UserTable extends Component {
    displayName = UserTable.name



    constructor(props) {
        super(props);
    }

    renderTableData() {
        var ID = 1;
        if (this.props.usergroup) {
            return this.props.usergroup.users.map((user, index) => {
                const { id, username } = user;
                // console.log(user.users[0]);
                return (
                    <tr id='table_row'>
                        <td id='userTable_tableContent'>{ID++}</td>
                        <td id='userTable_tableContent'>{username}</td>
                        <td id='userTable_tableContent'><img id='table__image' src={Bin} onClick={() => this.deleteRow(id)}/></td>
                    </tr>
                )
            })
        }
    }

    deleteRow(id) {
        var formData={
            userid:id,
            usergroupid: this.props.usergroup.id,

        }
        return fetch(UserGroupAPI+'deleteuserfromgroup', {
            method: 'delete',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(formData)
        })
        .then(response => response.json())
        .then(data => {
            this.props.onUserChanged(data);
        });
    }

    renderMergeOptions() {
        return this.props.users.map((user, index) => {
            const { id, username, firstname, lastname } = user;
            return (
                <option value={id}>{username}</option>
            )
        })
    }

    submitNewUser(event) {
        event.preventDefault();
        var formData = {
            usergroupid: this.props.usergroup.id,
            userid: event.target.elements.userid.value
        }
        fetch(UserGroupAPI + "addusertogroup", {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(data => {
                this.props.onUserChanged(data);
            })
    }

    render() {
        return (
            <table id='userTable'>
                <div id='userTable__title'>
                    Example User Group
                </div>
                <tbody>
                    <tr>
                        <th id='userTable_tableHeader'>ID</th>
                        <th id='userTable_tableHeader'>User</th>
                        <th id='userTable_tableHeader'>Delete</th>
                    </tr>
                </tbody>
                {this.renderTableData()}
                <div onClick={this.renderNewUserGroup}>Add new group</div>
                <div>
                    <form id='userTable_form' onSubmit={event => this.submitNewUser(event)}>
                        <select id="userTable_users" name="userid">
                            {this.renderMergeOptions()}
                        </select>
                        <button type='submit'>Submit</button>
                    </form>
                </div>
            </table>
        )
    }
}