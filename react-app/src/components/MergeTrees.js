import Cookies from 'universal-cookie';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import './MergeTrees.css';
import $ from 'jquery'

const cookies = new Cookies();
const GetFamiliesAPI = "https://localhost:5001/api/familytree/getfamilies?";
const AddTreeAPI = 'https://localhost:5001/api/familytree/mergetreebyadd';
const ReplaceTreeAPI = 'https://localhost:5001/api/familytree/mergetreebyreplace';


export class MergeTrees extends Component {
    displayName = MergeTrees.name

    constructor(props) {
        super(props);
        const urlParams = new URLSearchParams(window.location.search);

        const tree1 = urlParams.get('tree1');
        const tree2 = urlParams.get('tree2');

        this.state = {
            mergeType: '',
            tree1: tree1,
            tree2: tree2,
            families: []
        };
        // this.populateFamilyDetails();
        fetch(GetFamiliesAPI + 'id=' + this.state.tree1)
            .then(response => response.json())
            .then(data => {
                this.setState({ families: data });
                console.log(data);
            })
    }

    changeMergeState(event) {
        var button = event.target;
        this.state.mergeType = event.target.innerHTML;
        $(".mergeTrees_button").removeClass("mergeTrees_button_selected");
        $(button).addClass("mergeTrees_button_selected");
    }

    submit(event) {
        event.preventDefault();

        var request = {
            family1id: event.target.elements.family.value,
            treename: event.target.elements.treename.value,
            tree1id: this.state.tree1,
            tree2id: this.state.tree2
        };

        var endpoint = '';
        if (this.state.mergeType === 'Add') {
            endpoint = AddTreeAPI;
        }

        if (this.state.mergeType === 'Replace') {
            endpoint = ReplaceTreeAPI;
        }


        console.log(request);
        fetch(endpoint, {
            method: 'POST',
            body: JSON.stringify(request),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(data => {
                window.location.href = '/import'
            })
    }

    renderMergeOptions() {
        return this.state.families.map((family, index) => {
            const { id, father, mother } = family;
            return (
                <option value={id}>{this.renderDropDownList(father, mother)}</option>
            )
        })
    }

    renderDropDownList(father, mother) {
        var name = '';
        if (father != null) {
            name += father.name + " ";
        }

        if (mother != null) {
            name += mother.name;
        }

        return name;
    }

    populateFamilyDetails() {
        //get family details and IDs
        fetch(GetFamiliesAPI + 'id=' + this.state.tree2)
            .then(response => response.json())
            .then(data => {
                this.setState({ families: data });
                console.log(data);
            })

    }

    render() {
        return (
            <div id='mergeTrees_mainContent'>
                <div id='mergeTrees_content'>
                    <form onSubmit={event => this.submit(event)}>
                        <div id='mergeTree_title'>
                            Merge Trees
                        </div>

                        <div class='mergeTrees_caption'>
                            When merging trees, you can either add to an existing tree or replace an existing tree
                        </div>
                        <div id='mergeTrees_buttons'>
                            <div class='mergeTrees_button' value="add" onClick={event => this.changeMergeState(event)}>Add</div>
                            <div class='mergeTrees_button' value="replace" onClick={event => this.changeMergeState(event)}>Replace</div>
                            <div class='mergeTrees_button' value="mix" onClick={event => this.changeMergeState(event)}>Mix</div>
                        </div>

                        <div id='mergeTrees_section'>
                            <div id='mergeTrees_subheading'>
                                Select Child Family
                            </div>
                            <div class='mergeTrees_caption'>
                                Which child family would you like to add the tree to
                            </div>
                            <div >
                                <select id='mergeTrees_list' name="family">
                                    {this.renderMergeOptions()}
                                </select>
                            </div>
                        </div>

                        <div id='mergeTrees_section'>
                            <div id='mergeTrees_subheading'>
                                Tree name
                        </div>
                            <div class='mergeTrees_caption'>
                                Give the new tree a name.
                        </div>
                            <div>
                                <input type='text' id='mergeTrees_name' name='treename' />
                            </div>
                        </div>

                        <div id='mergeTrees_submitForm'>
                            <div id='mergeTrees_cancel'>
                                Cancel
                            </div>

                            <button type='submit' id='mergeTrees_merge'>
                                Merge
                            </button>
                        </div>
                    </form>
                </div>
            </div >
        )
    }
}