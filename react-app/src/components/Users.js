import React, { Component } from 'react';
import './Users.css';
import { Breadcrumb } from './Breadcrumb';
import { GroupTable } from './GroupTable';
import { UserTable } from './UserTable';
import Cookies from 'universal-cookie';

const UserGroupAPI = 'https://localhost:44556/api/usergroup/';
const UsersAPI = 'https://localhost:44556/api/authentication/';
const cookies = new Cookies();


export class Users extends Component {
    displayName = Users.name

    constructor(props) {
        super(props);
        this.state = { groups: [], users: [], paths: ["User Manager"], currentGroup: '0' };

        fetch(UserGroupAPI + "getusergroups?" + "accesstoken=" + cookies.get("name").accessToken)
            .then(response => response.json())
            .then(data => {
                this.setState({ groups: data });
            })
        this.handleGroupChange = this.handleGroupChange.bind(this);
        this.handleUserChange = this.handleGroupChange.bind(this);
        this.handleGroupSelectedChange = this.handleGroupSelectedChange.bind(this);


        fetch(UsersAPI + "getallusers")
            .then(response => response.json())
            .then(data => {
                this.setState({ users: data });
            });
    }

    handleGroupChange(newGroups) {
        this.setState({ groups: newGroups });
    }

    handleUserChange(newUsers) {
        this.setState({ users: newUsers });
    }

    handleGroupSelectedChange(index) {
        this.setState({ currentGroup: index });
    }

    render() {
        var users = this.state.groups == undefined ? null : <UserTable usergroup={this.state.groups[this.state.currentGroup]} users={this.state.users} onUserChanged={this.handleGroupChange} />
        return (
            <div id='mainContent'>
                <div>
                    <Breadcrumb record={this.state.paths} />
                </div>
                <div>
                    User Manager
                </div>
                <div>
                    Allow you to import your GEDCOM files into the system.
                    This page will also allow you to set the visibility of the resulting family tree as well as deciding  who can maintain the tree.
                </div>

                <div id='users_tables'>
                    <div id='users__table'>
                        <GroupTable groups={this.state.groups} onGroupSelected={this.handleGroupSelectedChange} onGroupChanged={this.handleGroupChange} />
                    </div>
                    <div id='users__table'>
                        {users}
                    </div>
                </div>
            </div>
        )
    }
}