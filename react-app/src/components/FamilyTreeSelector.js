import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ShortTile } from './ShortTile';
import './FamilyTreeSelector.css';
import { Breadcrumb } from './Breadcrumb';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

const API = 'https://localhost:5001/api/familytree/getallfamilies?';

export class FamilyTreeSelector extends Component {
    displayName = FamilyTreeSelector.name

    constructor(props) {
        super(props);
        if (!cookies.get("name")) {
            window.location.href = "/login";
        }
        this.state = { information: [], paths: ["Family Tree selector"] };
        console.log(cookies.get("name").id);
        fetch(API + "userid=" + cookies.get("name").id)
            .then(response => response.json())
            .then(data => {
                this.setState({ information: data, loading: false });
                console.log(data);

            })
    }


    familyHistory(info) {
        window.location.href = '/familyhistory?treeid=' + info.id;
    }

    renderTiles(information) {
        return (
            <div id='familyTreeSelector_Content'>
                {information.map(info => <ShortTile record={info} />)}
            </div>
        );
    }

    render() {
        return (
            <div id="bookProduction_MainContent">
                <div>
                    <Breadcrumb record={this.state.paths} />
                </div>

                <div id="bookProduction_Title">
                    Select family tree to view
                </div>

                <div>
                    {this.renderTiles(this.state.information)}
                </div>

                <div>
                </div>
            </div>
        );
    }
}
