import React, { Component } from 'react';
import { Breadcrumb } from './Breadcrumb';
import { FileInput } from './FileInput';
import { Table } from './Table';
import { SelectTrees } from './SelectTrees';
import './Import.css';
import Cookies from 'universal-cookie';

const cookies = new Cookies();


const GetFilesAPI = 'https://localhost:46397/api/import?';

export class Import extends Component {
  displayName = Import.name

  constructor(props) {
    super(props);
    this.state = { files: [], paths: ["Import"] };
    if (!cookies.get("name")) {
      window.location.href = "/login";
    }
    fetch(GetFilesAPI + "accesstoken=" + cookies.get("name").accessToken)
      .then(response => response.json())
      .then(data => {
        this.setState({ files: data });
      })

    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(newFiles) {
    this.setState({ files: newFiles });
    console.log(newFiles);
  }

  render() {
    return (
      <div id="mainContent">
        <div>
          <Breadcrumb record={this.state.paths} />
        </div>
        <div id="import_content">
          <div>
            <div id="import_settings">
              <div id="import_title">
                Upload GEDCOM
              </div>
              <div>
                <FileInput onFileChange={this.handleChange}/>
              </div>
            </div>
            <div id="import_settings">
              <div id="import_title">
                Merge Trees
              </div>
              <div>
                <SelectTrees />
              </div>
            </div>
          </div>


          <div id="import_table">
            <div id="import_title">
              Imported GEDCOMs
          </div>
            <div>
              <Table files={this.state.files} onFileChange={this.handleChange} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}