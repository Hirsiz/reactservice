import React, { Component } from 'react';
import { Tile } from './Tile';
import './FamilyBlock.css';

export class FamilyBlock extends Component {
    displayName = FamilyBlock.name

    renderIndividualFamily(family) {
        return (
            <div id="">
                <div id='familyBlock__parent'>
                    {/* <b>Father</b> <br /> */}
                    Name: {family.father.name} <br />
                    Gender: {family.father.gender} <br />
                    Date of Birth: {family.father.birth.date} <br />
                    Place of Birth: {family.father.birth.place} <br />
                    Date of Death: {family.father.death.date} <br />
                    Place of Death: {family.father.death.place} <br />
                </div>
                <div id='familyBlock__parent'>
                    {/* <b>Mother</b> <br /> */}
                    Name: {family.mother.name} <br />
                    Gender: {family.mother.gender} <br />
                    Date of Birth: {family.mother.birth.date} <br />
                    Place of Birth: {family.mother.birth.place} <br />
                    Date of Death: {family.mother.death.date} <br />
                    Place of Death: {family.mother.death.place} <br />
                </div>
                <br />
                <br />
                {/* <div>
                    Children are: {family.childrenFamily.map(children => <div>- {children.name} </div>)}
                </div> */}
                <br />
            </div>
        )
    }

    renderChildren(child) {
        return (
            <div>
                Child <br />
                Name: {child.name} <br />
                Gender: {child.gender} <br />
                Date of Birth{child.birth.date} <br />
                Place of Birth{child.birth.place} <br />
                Date of Death{child.death.date} <br />
                Place of Death{child.death.place} <br />
            </div>
        )
    }

    render() {
        var families = this.props.record.family;
        console.log(families);
        return (
            <div id="familyBlock__container">
                <div id='familyBlock_title'>
                    {this.props.record.family[0].father.name} Family Tree
                </div>
                <br />
                <br />
                {families.map(family => this.renderIndividualFamily(family))}
            </div>
        )
    }
}