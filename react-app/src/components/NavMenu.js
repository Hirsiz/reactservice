﻿import React, { Component } from 'react';
import './NavMenu.css';
import logo from './noun_Pen_2144033.svg'
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies();


export class NavMenu extends Component {
  displayName = NavMenu.name

  render() {
    var ctaAction = cookies.get("name") ?
      <Link class="navOption2" to="/logout">
        Logout
      </Link> :
      <Link class="navOption2" to="/login">
        Sign In
      </Link>

    var navOptions = window.location.pathname != '/landing' ?
      <div>
        <Link class="navOption" to="/import">
          Import
      </Link>
        <Link class="navOption" to="/familytreeselector">
          Family History
      </Link>
        <Link class="navOption" to="/bookproduction">
          Books
      </Link>
        {ctaAction}
      </div>
      :
      <div>
        <Link class="navOption" to="/landing">
          About
        </Link>
        <Link class="navOption" to="/login">
          Sign In
        </Link>
        <Link class="navOption2" to="/createaccount">
          Register
        </Link>
      </div>

    return (
      <div id="nav">
        <div id="navHeader">
          <Link id="container" to="/">
            <img src={logo} alt="logo" id="logo" />
            INDIVIDUAL PROJECT
          </Link>
          <div id="navOptions">
            {navOptions}
          </div>
        </div>
      </div>
    );
  }
}
