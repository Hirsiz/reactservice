import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FamilyHistory } from './components/FamilyHistory';
import { Import } from './components/Import';
import { BookProduction } from './components/BookProduction'
import { Login } from './components/Login'
import { Landing } from './components/Landing'
import { CreateAccount } from './components/CreateAccount'
import { Logout } from './components/Logout'
import { MergeTrees } from './components/MergeTrees'
import { FamilyTreeSelector } from './components/FamilyTreeSelector'
import { Users } from './components/Users'

export default class App extends Component {
  displayName = App.name

  render() {
    return (
      <div>
        <div>
        </div>
        <div>
          <Layout>
            <Route exact path='/' component={Home} />
            <Route path='/import' component={Import} />
            <Route path='/familyhistory' component={FamilyHistory} />
            <Route path='/bookproduction' component={BookProduction} />
            <Route path='/createaccount' component={CreateAccount} />
            <Route path='/logout' component={Logout} />
            <Route path='/landing' component={Landing} />
            <Route path='/login' component={Login} />
            <Route path='/mergetrees' component={MergeTrees} />
            <Route path='/familytreeselector' component={FamilyTreeSelector} />
            <Route path='/users' component={Users} />

          </Layout>
        </div>
      </div>
    );
  }
}
